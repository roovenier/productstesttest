//
//  StoreFrontController.swift
//  ProductsTestTest
//
//  Created by alexander.oschepkov on 28.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

import UIKit
import CSwiftV
import CoreData

struct Constants {
    static let backendProductUpdateNotification = "backendProductUpdateNotification"
    static let storefrontProductUpdateNotification = "storefrontProductUpdateNotification"
}

class StoreFrontController: UIViewController, ProductViewDelegate, UIScrollViewDelegate {
    
    class func instanceProductViewFromNib() -> UIView {
        return UINib(nibName: "ProductView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ProductView
    }

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var managedObjectContext = DataManager.instance.persistentContainer.viewContext
    var productsArray = [Product]()
    var arrayWithExistingProducts = [Product]()
    var previousPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.delaysContentTouches = false
        
        self.initializeData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reformProductCarouselOnRotate), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateProductFromNotification), name: Notification.Name.init(Constants.storefrontProductUpdateNotification), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.size.width;
        let fractionalPage = scrollView.contentOffset.x / pageWidth;
        let page = lroundf(Float(fractionalPage))
        
        if previousPage != page {
            previousPage = page
            self.pageControl.currentPage = page
        }
    }
    
    // MARK: Actions
    
    func updateProductFromNotification(notification: Notification) {
        let product = notification.object as? Product
        
        self.updateProductView(product: product!)
    }
    
    func initializeData() {
        self.productsArray = self.productsFromStore()
        self.arrayWithExistingProducts = self.productsArray.filter {$0.amount > 0}
        
        self.pageControl.numberOfPages = arrayWithExistingProducts.count
        
        self.formProductCarousel()
    }
    
    func formProductCarousel() {
        for i in 0..<self.arrayWithExistingProducts.count {
            let productItem = self.arrayWithExistingProducts[i]
            
            let productView = StoreFrontController.instanceProductViewFromNib() as! ProductView
            let xPosition = self.view.frame.width * CGFloat(i)
            productView.frame = CGRect(x: xPosition, y: self.scrollView.frame.origin.y, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
            
            productView.productTitleLabel.text = productItem.title
            productView.productPriceLabel.text = String(format: "%.02f", locale: Locale.current, arguments: [productItem.price])
            productView.productAmountLabel.text = String(productItem.amount)
            
            productView.delegate = self
            productView.product = productItem
            
            self.scrollView.contentSize.width = self.scrollView.frame.width * CGFloat(i + 1)
            self.scrollView.addSubview(productView)
        }
    }
    
    func reformProductCarouselOnRotate() {
        let previousPage = self.previousPage
        
        for i in 0..<self.scrollView.subviews.count {
            let productView = self.scrollView.subviews[i]
            
            let xPosition = self.view.frame.width * CGFloat(i)
            productView.frame = CGRect(x: xPosition, y: self.scrollView.frame.origin.y, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
            
            self.scrollView.contentSize.width = self.scrollView.frame.width * CGFloat(i + 1)
        }
        
        let newOffset = CGFloat(previousPage) * self.scrollView.frame.width
        self.scrollView.setContentOffset(CGPoint(x: newOffset, y: self.scrollView.frame.origin.y), animated: false)
        
        self.previousPage = previousPage
        self.pageControl.currentPage = previousPage
    }
    
    func updateProductView(product: Product) {
        let productView = self.scrollView.subviews.first(where: {
            let productView = $0 as! ProductView
            
            return productView.product == product
        }) as? ProductView
        
        productView?.productTitleLabel.text = product.title
        productView?.productPriceLabel.text = String(format: "%.02f", locale: Locale.current, arguments: [product.price])
        productView?.productAmountLabel.text = String(product.amount)
        
        if let productIndex = self.arrayWithExistingProducts.index(of: product) {
            if product.amount == 0 {
                self.arrayWithExistingProducts.remove(at: productIndex)
                
                self.removeViewAndReformScrollView(view: productView!, indexOfRemovedProduct: productIndex)
            }
        }
        
        if productView == nil && product.amount > 0 {
            self.addViewAndReformScrollView(product: product)
        }
    }
    
    func productsFromStore() -> Array<Product> {
        let request: NSFetchRequest<Product> = Product.fetchRequest()
        
        do {
            let results = try self.managedObjectContext.fetch(request)
            return results
        } catch {
            print("Fetch error: \(error)")
            return []
        }
    }
    
    func purchaseProduct(view: ProductView) {
        let productItem = view.product
        
        productItem?.amount -= 1
        
        view.productAmountLabel.text = String(productItem!.amount)
        
        let productIndex = self.arrayWithExistingProducts.index(of: productItem!)
        
        if productItem?.amount == 0 {
            self.arrayWithExistingProducts.remove(at: productIndex!)
            
            self.removeViewAndReformScrollView(view: view, indexOfRemovedProduct: productIndex!)
        }
        
        NotificationCenter.default.post(name: Notification.Name.init(Constants.backendProductUpdateNotification), object: productItem)
        
        // Задержка в 3 секунды из задачи
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            do {
                try self.managedObjectContext.save()
            } catch {
                print("Could not update the product")
            }
        }
    }
    
    func removeViewAndReformScrollView(view: ProductView, indexOfRemovedProduct: Int) {
        view.removeFromSuperview()
        
        let slidesQuantity = self.scrollView.subviews.count
        
        self.pageControl.numberOfPages = slidesQuantity;
        
        let oldOffset = self.scrollView.contentOffset.x
        
        for i in 0..<slidesQuantity {
            let productView = self.scrollView.subviews[i]
            
            let xPosition = self.view.frame.width * CGFloat(i)
            productView.frame = CGRect(x: xPosition, y: self.scrollView.frame.origin.y, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
        }
        
        var currentPage = 0
        
        if(slidesQuantity > 1 && indexOfRemovedProduct > 0) {
            let newOffset = oldOffset - self.scrollView.frame.width
            self.scrollView.setContentOffset(CGPoint(x: newOffset, y: self.scrollView.frame.origin.y), animated: false)
            
            let pageWidth: CGFloat = scrollView.frame.size.width;
            let fractionalPage = scrollView.contentOffset.x / pageWidth;
            let page = lroundf(Float(fractionalPage))
            currentPage = page
        }
        
        self.pageControl.currentPage = currentPage
        self.scrollView.contentSize.width = self.scrollView.frame.width * CGFloat(slidesQuantity)
    }
    
    func addViewAndReformScrollView(product: Product) {
        var productIndex = self.productsArray.index(of: product)
        
        if productIndex! > self.arrayWithExistingProducts.count {
            productIndex = self.arrayWithExistingProducts.count
        }
        
        self.arrayWithExistingProducts.insert(product, at: productIndex!)
        
        let productView = StoreFrontController.instanceProductViewFromNib() as! ProductView
        let xPosition = self.view.frame.width * CGFloat(productIndex!)
        productView.frame = CGRect(x: xPosition, y: self.scrollView.frame.origin.y, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
        
        productView.productTitleLabel.text = product.title
        productView.productPriceLabel.text = String(format: "%.02f", locale: Locale.current, arguments: [product.price])
        productView.productAmountLabel.text = String(product.amount)
        
        productView.delegate = self
        productView.product = product
        
        self.scrollView.contentSize.width += self.scrollView.frame.width
        self.scrollView.insertSubview(productView, at: productIndex!)
        self.pageControl.numberOfPages += 1
        
        for i in productIndex!+1..<self.scrollView.subviews.count {
            let productView = self.scrollView.subviews[i]
            
            let xPosition = self.view.frame.width * CGFloat(i)
            productView.frame = CGRect(x: xPosition, y: self.scrollView.frame.origin.y, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
        }
    }
}
