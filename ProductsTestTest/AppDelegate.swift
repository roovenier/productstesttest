//
//  AppDelegate.swift
//  ProductsTestTest
//
//  Created by alexander.oschepkov on 28.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

import UIKit
import CSwiftV
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let defaults = UserDefaults.standard
        let isPreloaded  = defaults.bool(forKey: "isPreloaded")
        if(!isPreloaded) {
            self.settingInitialData()
            defaults.set(true, forKey: "isPreloaded")
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        DataManager.instance.saveContext()
    }

    // MARK: Actions
    
    func settingInitialData() {
        let managedObjectContext = DataManager.instance.persistentContainer.viewContext
        
        self.dataFromCSV(managedObjectContext: managedObjectContext)
    }
    
    func dataFromCSV(managedObjectContext: NSManagedObjectContext) {
        let csvPath = Bundle.main.path(forResource: "data", ofType: "csv")
        let csvContent = try? NSString(contentsOfFile: csvPath! as String, encoding: String.Encoding.utf8.rawValue)
        
        for item in CSwiftV(with: csvContent! as String).rows {
            
            let newObject = Product(context: managedObjectContext)
            newObject.title = String(item[0])
            newObject.price = Float(item[1])!
            newObject.amount = Int16(item[2])!
            
            do {
                try managedObjectContext.save()
            } catch let error as NSError {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
}

