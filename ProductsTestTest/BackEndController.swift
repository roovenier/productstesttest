//
//  BackEndController.swift
//  ProductsTestTest
//
//  Created by alexander.oschepkov on 28.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

import UIKit
import CSwiftV
import CoreData

class BackEndController: UIViewController, UITableViewDelegate, UITableViewDataSource, EditingControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var managedObjectContext = DataManager.instance.persistentContainer.viewContext
    var productsArray = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateProductFromNotification), name: Notification.Name.init(Constants.backendProductUpdateNotification), object: nil)
        
        self.productsArray = self.productsFromStore()
        
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productsArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell")
        
        if(cell == nil) {
            cell = UITableViewCell.init(style: .value1, reuseIdentifier: "ProductCell")
        }
        
        let productItem = self.productsArray[indexPath.row]
        
        cell?.textLabel?.text = productItem.title
        cell?.detailTextLabel?.text = String(describing: productItem.amount)
        
        cell?.accessoryType = .disclosureIndicator
        
        return cell!;
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = self.productsArray[indexPath.row]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditingController") as? EditingController
        vc?.product = product
        vc?.delegate = self
        vc?.indexPath = indexPath
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    // MARK: Actions
    
    func updateProductFromNotification(notification: Notification) {
        let product = notification.object as? Product
        
        let productIndex = self.productsArray.index(of: product!)
        
        self.productsArray[productIndex!] = product!
        
        self.tableView.reloadData()
    }
    
    func productsFromStore() -> Array<Product> {
        let request: NSFetchRequest<Product> = Product.fetchRequest()
        do {
            let results = try self.managedObjectContext.fetch(request)
            return results
        } catch {
            print("Fetch error: \(error)")
            return []
        }
    }
    
    func getCSVArray() -> Array<Any> {
        let csvPath = Bundle.main.path(forResource: "data", ofType: "csv")
        let csvContent = try? NSString(contentsOfFile: csvPath! as String, encoding: String.Encoding.utf8.rawValue)
        
        return CSwiftV(with: csvContent! as String).rows
    }
    
    // MARK: EditingControllerDelegate
    
    func updateProduct(product: Product, indexPath: IndexPath) {
        _ = self.navigationController?.popViewController(animated: true)
        
        NotificationCenter.default.post(name: Notification.Name.init(Constants.storefrontProductUpdateNotification), object: product)
        
        // Задержка в 5 секунд из задачи
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            do {
                try self.managedObjectContext.save()
                
                self.productsArray[indexPath.row] = product
                
                self.tableView.reloadData()
            } catch {
                fatalError("Failure to update product: \(error)")
            }
        }
    }
}
