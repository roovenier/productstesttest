//
//  EditingController.swift
//  ProductsTestTest
//
//  Created by alexander.oschepkov on 28.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

import UIKit

protocol EditingControllerDelegate {
    func updateProduct(product: Product, indexPath: IndexPath)
}

class EditingController: UITableViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var amountTextLabel: UILabel!

    var product: Product?
    var delegate: EditingControllerDelegate?
    var indexPath: IndexPath?
    var initialAmount: Int16?
    var isInitialAmountChanged: Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.product!.title
        let saveButton = UIBarButtonItem.init(barButtonSystemItem: .save, target: self, action: #selector(self.saveProduct(_:)))
        self.navigationItem.rightBarButtonItem = saveButton
        
        self.titleTextField.text = self.product!.title
        self.priceTextField.text = String(self.product!.price)
        self.amountTextLabel.text = String(self.product!.amount)
        
        self.initialAmount = self.product?.amount
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.isInitialAmountChanged! == false {
            self.amountTextLabel.text = String(self.product!.amount)
        }
    }
    
    // MARK: Actions
    
    func saveProduct(_ sender: UIBarButtonItem) {
        self.isInitialAmountChanged = true
        
        if let title = self.titleTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
            let price = self.priceTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            
            if title.characters.count != 0 && price.characters.count != 0 {
                self.product?.title = title
                self.product?.price = Float(price)!
                self.product?.amount = self.initialAmount!
                
                self.delegate?.updateProduct(product: self.product!, indexPath: self.indexPath!)
            } else {
                let alert = UIAlertController(title: "Notice", message: "Please fill in all fields", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func increaseAmountAction(_ sender: UIButton) {
        self.initialAmount = self.initialAmount! + 1
        
        self.amountTextLabel.text = String(self.initialAmount!)
    }
    
    @IBAction func decreaseAmountAction(_ sender: UIButton) {
        if self.initialAmount! > 0 {
            self.initialAmount = self.initialAmount! - 1
            
            self.amountTextLabel.text = String(self.initialAmount!)
        }
    }
}
