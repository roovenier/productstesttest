//
//  ProductView.swift
//  ProductsTestTest
//
//  Created by alexander.oschepkov on 31.07.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

import UIKit

protocol ProductViewDelegate {
    func purchaseProduct(view: ProductView);
}

class ProductView: UIView {
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productAmountLabel: UILabel!
    
    var delegate: ProductViewDelegate?
    var product: Product?
    
    @IBAction func buyProductAction(_ sender: UIButton) {
        self.delegate?.purchaseProduct(view: self)
    }
}
